# QT Dark Theme (for Dolphin)

This is a project which aims at converting QDarkStyleSheet into a format that is compatible with Dolphin.

Any issues, suggestions, contributions, or questions about the original project should be conducted in the channels designated by the original project.
This project is *only* intended to provide Dolphin compatibility.

## Installation

### Copy the stylesheet

Copy `qt-dark.qss` to `%DOLPHIN_USER_DIRECTORY%/Styles/` where `DOLPHIN_USER_DIRECTORY` is the directory that your user data is stored in (settings, controller maps, etc.).
On Windows the default is `%userprofile%/Documents/Dolphin Emulator`.

You can safely rename `qt-dark.qss` to anything that you want.

> The user directory is *NOT* where Dolphin is installed to. If you see the Dolphin executable, then you are in the wrong directory.

### Copy the icons

Locate the directory where Dolphin is installed.
This is the directory where you will find the Dolphin executable and we will refer to it as `DOLPHIN_ROOT`.

Create a new directory in `DOLPHIN_ROOT` and name it `qt-dark`.
Copy `rc` to `%DOLPHIN_ROOT%/qt-dark/`.

> These directory and file names must be *exactly* as they appear here.

### Configure Dolphin to load the custom stylesheet

Open Dolphin and go to Options > Configuration > Interface.
Ensure that the option "Use Custom User Style" is checked.
In the "User Style" dropdown, select "qt-dark", or if you renamed the stylesheet, select your custom name.

## Testing

The latest tested version is `5.0-7988`.

## Acknowledgements

Thanks to [Kawasuzu](https://forums.dolphin-emu.org/User-kawasuzu) for figuring all this stuff out.
